package com.parkermc.amnesia.items;

import com.parkermc.amnesia.ModMain;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemAmnesiaClock extends Item {
	public static final String unlocalizedName = "amnesia_clock";
	
	public ItemAmnesiaClock() {
		super();
		this.setCreativeTab(ModMain.tab);
		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);
	}
		
	@SideOnly(Side.CLIENT)
	public void initModel() {
		ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(this.getRegistryName(), "inventory"));
	}
}
