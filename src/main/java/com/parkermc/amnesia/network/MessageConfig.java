package com.parkermc.amnesia.network;

import com.google.gson.Gson;
import com.parkermc.amnesia.Config;
import com.parkermc.amnesia.proxy.ProxyClient;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MessageConfig implements IMessage {
	private static Gson GSON = new Gson();
	
	Config config;
	
	public MessageConfig(){
	}
	
	public MessageConfig(Config config){
		this.config = config;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.config = GSON.fromJson(ByteBufUtils.readUTF8String(buf), Config.class);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, GSON.toJson(this.config));
	}
	
	public static class Handler implements IMessageHandler<MessageConfig, IMessage>{

		@SideOnly(Side.CLIENT)
		@Override
		public IMessage onMessage(MessageConfig message, MessageContext ctx) {
			ProxyClient.serverConfig = message.config;
			return null;
		}
		
	}
}
