package com.parkermc.amnesia.network;

import com.parkermc.amnesia.proxy.ProxyClient;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MessageStartTick implements IMessage {	
	private long start_tick;
	
	public MessageStartTick(){
	}
	
	public MessageStartTick(long time){
		this.start_tick = time;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.start_tick = buf.readLong();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(this.start_tick);
	}
	
	public static class Handler implements IMessageHandler<MessageStartTick, IMessage>{

		@SideOnly(Side.CLIENT)
		@Override
		public IMessage onMessage(MessageStartTick message, MessageContext ctx) {
			ProxyClient.amnesia_start_tick = message.start_tick;
			return null;
		}
		
	}
}
