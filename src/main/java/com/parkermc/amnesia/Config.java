package com.parkermc.amnesia;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Config {
	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	public String random_message = "[Amnesia] You have forgotten what you have once known";
	public String cure_message = "[Amnesia] You happen to remember things";
	public String normal_message = "[Amnesia] You have regained your memory";
	public String post_change_message = "";
	public long expire_ticks = 6000; // five mins 
	
	public BlockMemories block_of_memories = new BlockMemories();
	
	public class BlockMemories {
		public String hover_text = ""; 
		public int lootchest_weight = 5;
		public int lootchest_quality = 5;
		
		public int ore_gen_min_height = 0;
		public int ore_gen_max_height = 32;
		public int ore_gen_chance = 7;
		public int ore_gen_max_per_vein = 5;
	}
	
	public Gui gui = new Gui();
	
	public class Gui{
		public int horizontal = -1;
		public int vertical = -1;
	}
	
	public static Config Read(File file) throws IOException {
		if(!file.exists()) {
			file.createNewFile();
			Config obj = new Config();
			obj.Write(file);
			return obj;
		}
		return GSON.fromJson(new FileReader(file), Config.class);
		
	}
	
	public void Write(File file) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		writer.write(GSON.toJson(this));
		writer.close();
	}
}
