package com.parkermc.amnesia;

import com.parkermc.amnesia.commands.CommandAmnesia;
import com.parkermc.amnesia.network.MessageConfig;
import com.parkermc.amnesia.network.MessageStartTick;
import com.parkermc.amnesia.proxy.ProxyCommon;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = ModMain.MODID,  useMetadata = true)
public class ModMain{
    public static final String MODID = "amnesia";
    
    @SidedProxy(clientSide = "com.parkermc.amnesia.proxy.ProxyClient", serverSide = "com.parkermc.amnesia.proxy.ProxyServer", modId = MODID)
    public static ProxyCommon proxy;
    
    public static SimpleNetworkWrapper network;
    
    public static ModTab tab = new ModTab("amnesia");
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
    	proxy.preInit(event);
    	network = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);
    	network.registerMessage(MessageConfig.Handler.class, MessageConfig.class, 0, Side.CLIENT);
    	network.registerMessage(MessageStartTick.Handler.class, MessageStartTick.class, 1, Side.CLIENT);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	proxy.init(event);    
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	proxy.postInit(event);
    }
    
    @EventHandler
    public void serverLoad(FMLServerStartingEvent event){
    	event.registerServerCommand(new CommandAmnesia());
    }

}

