package com.parkermc.amnesia.events;

public interface IAmnesiaEvents{
	public void random() ;
	public void normal();
	public void updatePost();
	public void updatePostClient();
	public void cure();
}