package com.parkermc.amnesia.events;

import com.parkermc.amnesia.ModMain;
import com.parkermc.amnesia.data.DataAmnesia;
import com.parkermc.amnesia.network.MessageStartTick;
import com.parkermc.amnesia.proxy.ProxyCommon;

import net.minecraftforge.common.DimensionManager;

@AmnesiaEvent
public class EventUpdateTime implements IAmnesiaEvents{
	
	@Override
	public void random() {
	}

	@Override
	public void normal() {
	}

	@Override
	public void cure() {
		if(!DimensionManager.getWorld(0).isRemote) {
			DataAmnesia.instance.lastReset += ProxyCommon.config.expire_ticks;
			DataAmnesia.instance.save();
			ModMain.network.sendToAll(new MessageStartTick(DataAmnesia.instance.lastReset));
		}
	}
	
	@Override
	public void updatePost() {
		if(!DimensionManager.getWorld(0).isRemote) {
			DataAmnesia.instance.lastReset = DataAmnesia.instance.world.getTotalWorldTime();
			DataAmnesia.instance.save();
			ModMain.network.sendToAll(new MessageStartTick(DataAmnesia.instance.lastReset));
		}
	}

	@Override
	public void updatePostClient() {
	}
}
