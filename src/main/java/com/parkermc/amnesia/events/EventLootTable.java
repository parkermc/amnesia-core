package com.parkermc.amnesia.events;

import com.parkermc.amnesia.ModBlocks;
import com.parkermc.amnesia.proxy.ProxyCommon;

import net.minecraft.item.Item;
import net.minecraft.world.storage.loot.LootEntry;
import net.minecraft.world.storage.loot.LootEntryItem;
import net.minecraft.world.storage.loot.conditions.LootCondition;
import net.minecraft.world.storage.loot.functions.LootFunction;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventLootTable {
	
	@SubscribeEvent
	public void lootLoad(LootTableLoadEvent event) {
		LootEntry entry = new LootEntryItem(Item.getItemFromBlock(ModBlocks.memories), ProxyCommon.config.block_of_memories.lootchest_weight, ProxyCommon.config.block_of_memories.lootchest_quality, new LootFunction[0], new LootCondition[0], ModBlocks.memories.getRegistryName().toString());
	    if (event.getName().toString().startsWith("minecraft:chests/")) {
	    	event.getTable().getPool("main").addEntry(entry);
	    }
	}
}
