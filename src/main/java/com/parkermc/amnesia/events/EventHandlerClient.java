package com.parkermc.amnesia.events;

import com.parkermc.amnesia.ModItems;
import com.parkermc.amnesia.ModToolsClient;
import com.parkermc.amnesia.gui.GuiOverlay;
import com.parkermc.amnesia.proxy.ProxyClient;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientDisconnectionFromServerEvent;
import net.minecraftforge.fml.relauncher.Side;

public class EventHandlerClient {

	private static boolean show_clock = false;

	@SubscribeEvent
	public void onDisconnect(ClientDisconnectionFromServerEvent event) {
		ProxyClient.serverConfig = null;
		ProxyClient.amnesia_start_tick = -1;
	}
	
	@SubscribeEvent
	public void onPlayerTick(TickEvent.PlayerTickEvent event) {
		if(event.side == Side.CLIENT && event.player.equals(ModToolsClient.getPlayer(Minecraft.getMinecraft()))) {
			if(event.player.getHeldItemMainhand() != null && event.player.getHeldItemMainhand().getItem() != null &&
					event.player.getHeldItemMainhand().getItem().getRegistryName().equals(ModItems.amnesia_clock.getRegistryName())) {
				show_clock = true;
			}else if(show_clock) {
				show_clock = false;
			}
		}
	}
	
    @SubscribeEvent
    public void onRenderGui(RenderGameOverlayEvent.Post event){
    	if (event.getType() == ElementType.EXPERIENCE) {
    		if(show_clock) {
    			new GuiOverlay(Minecraft.getMinecraft());
    		}
    	}
    }
}
