package com.parkermc.amnesia;

import com.parkermc.amnesia.items.*;

import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModItems {
	public static ItemReset reset;
	public static ItemCure cure;
	public static ItemAmnesiaClock amnesia_clock;
	
	public static void preInit() {
		GameRegistry.register(reset = new ItemReset());
		GameRegistry.register(cure = new ItemCure());
		GameRegistry.register(amnesia_clock = new ItemAmnesiaClock());
	}
	
	public static void initModels() {
		reset.initModel();
		cure.initModel();
		amnesia_clock.initModel();
	}
}
