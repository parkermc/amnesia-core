package com.parkermc.amnesia;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes {
	
	public static void preInit() {
		GameRegistry.addShapedRecipe(new ItemStack(ModItems.reset),
			" M ",
			"MBM",
			" M ",
			'M', ModBlocks.memories,
			'B', Blocks.BEACON
		);
		
		GameRegistry.addShapedRecipe(new ItemStack(ModItems.cure),
			"MMM",
			"MSM",
			"MMM",
			'M', ModBlocks.memories,
			'S', Items.NETHER_STAR
			);
		
		GameRegistry.addShapedRecipe(new ItemStack(ModItems.amnesia_clock), 
				"RrR",
				"rCr",
				"RrR",
				'R', ModItems.cure,
				'r', ModItems.reset,
				'C', Items.CLOCK
				);
		
		GameRegistry.addShapedRecipe(new ItemStack(ModBlocks.memories_compressed_1), 
				"MMM",
				"MMM",
				"MMM",
				'M', ModBlocks.memories
				);
		
		GameRegistry.addShapedRecipe(new ItemStack(ModBlocks.memories_compressed_2), 
				"MMM",
				"MMM",
				"MMM",
				'M', ModBlocks.memories_compressed_1
				);
		
		GameRegistry.addShapedRecipe(new ItemStack(ModBlocks.memories_compressed_3), 
				"MMM",
				"MMM",
				"MMM",
				'M', ModBlocks.memories_compressed_2
				);
		
		
		GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.memories, 9), new ItemStack(ModBlocks.memories_compressed_1));
		GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.memories_compressed_1, 9), new ItemStack(ModBlocks.memories_compressed_2));
		GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.memories_compressed_2, 9), new ItemStack(ModBlocks.memories_compressed_3));
	}
}
