package com.parkermc.amnesia;

import com.parkermc.amnesia.blocks.*;

import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModBlocks {
	public static BlockGoldenSoulSand golden_soul_sand;
	public static BlockMemories memories;
	public static BlockMemoriesCompressed memories_compressed_1;
	public static BlockMemoriesCompressed memories_compressed_2;
	public static BlockMemoriesCompressed memories_compressed_3;
	
	public static void preInit() {
		GameRegistry.register(golden_soul_sand = new BlockGoldenSoulSand());
		GameRegistry.register(new ItemBlock(golden_soul_sand), golden_soul_sand.getRegistryName());
		GameRegistry.register(memories = new BlockMemories());
		GameRegistry.register(new ItemBlock(memories), memories.getRegistryName());
		GameRegistry.register(memories_compressed_1 = new BlockMemoriesCompressed(1));
		GameRegistry.register(new ItemBlock(memories_compressed_1), memories_compressed_1.getRegistryName());
		GameRegistry.register(memories_compressed_2 = new BlockMemoriesCompressed(2));
		GameRegistry.register(new ItemBlock(memories_compressed_2), memories_compressed_2.getRegistryName());
		GameRegistry.register(memories_compressed_3 = new BlockMemoriesCompressed(3));
		GameRegistry.register(new ItemBlock(memories_compressed_3), memories_compressed_3.getRegistryName());
	}
	
	public static void initModels() {
		golden_soul_sand.initModel();
		memories.initModel();
		memories_compressed_1.initModel();
		memories_compressed_2.initModel();
		memories_compressed_3.initModel();
	}
	
	public static void updateConfigData() {
	}

}

