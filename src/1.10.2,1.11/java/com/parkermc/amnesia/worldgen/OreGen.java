package com.parkermc.amnesia.worldgen;

import java.util.Random;

import com.parkermc.amnesia.ModBlocks;
import com.parkermc.amnesia.proxy.ProxyCommon;

import net.minecraft.block.state.pattern.BlockMatcher;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkGenerator;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

public class OreGen implements IWorldGenerator{

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
		if(world.provider.getDimension() == 0) {
			if (ProxyCommon.config.block_of_memories.ore_gen_min_height < 0 || ProxyCommon.config.block_of_memories.ore_gen_max_height > 256 || ProxyCommon.config.block_of_memories.ore_gen_min_height > ProxyCommon.config.block_of_memories.ore_gen_max_height) {
				throw new IllegalArgumentException("Illegal Height Arguments for WorldGenerator");
			}

			WorldGenMinable generator = new WorldGenMinable(ModBlocks.memories.getDefaultState(), ProxyCommon.config.block_of_memories.ore_gen_max_per_vein, BlockMatcher.forBlock(Blocks.STONE));
			int heightdiff = ProxyCommon.config.block_of_memories.ore_gen_max_height - ProxyCommon.config.block_of_memories.ore_gen_min_height +1;
			for (int i=0; i<ProxyCommon.config.block_of_memories.ore_gen_chance; i++){
				int x = chunkX * 16 +random.nextInt(16);
				int y = ProxyCommon.config.block_of_memories.ore_gen_min_height + random.nextInt(heightdiff);
				int z = chunkZ * 16 + random.nextInt(16);

				generator.generate(world, random, new BlockPos(x, y, z));
			}

		}
	}

}
